// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';

// @Component({
//   selector: 'app-orglogin',
//   templateUrl: './orglogin.component.html',
//   styleUrls: ['./orglogin.component.css']
// })
// export class OrgloginComponent implements OnInit {
//   loginId: string;
//   password: string;

//   constructor(private router:Router) { 
//     this.loginId = '';
//     this.password = '';
//   }

//   ngOnInit(): void {
//   }
//   loginSubmit(loginForm:any):void {
//     console.log(loginForm)
//     //console.log("User ID : " + this.loginId);
//     //console.log("Password : " + this.password);

//    // if(this.loginId === 'manager' && this.password === '123') {
//      // alert("Welcome to Home page");
//     //}
//     //else{
//     //  alert('Login Failure');
//     }
//     newLogin(){
//       console.log("working");
//       this.router.navigateByUrl('/registerO')
//     }
//     }


     import { Component, OnInit } from '@angular/core';
     import { OrgService } from '../org.service';
     import { HttpClient } from '@angular/common/http';
     import {Router} from '@angular/router';
    
     @Component({
       selector: 'app-orglogin',
       templateUrl: './orglogin.component.html',
       styleUrls: ['./orglogin.component.css']
     })
     export class OrgloginComponent implements OnInit {
       Organizers : any;
    
       constructor(private service:OrgService,private httpClient:HttpClient,private router:Router) { 
         this.Organizers ={email:"",password:""};
       }
    
       ngOnInit(): void {
       }
       
       async loginOrg(loginForm:any){
         await this.service.getOrgByEmailAndPassword(loginForm).then((result:any)=>{console.log(result);
           this.Organizers = result
         });
         this.service.setUserLoggedIn();
         if(this.Organizers != null){
           
           this.router.navigate(['orghomepage']);
         }
         else{
           alert("Invalid User");
         }
       }
       }

    
    
      