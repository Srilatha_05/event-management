
import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { OrgService } from '../org.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-orgregister',
  templateUrl: './orgregister.component.html',
  styleUrls: ['./orgregister.component.css']
})
export class OrgregisterComponent implements OnInit {

  Organizers : any;

  constructor(private httpClient:HttpClient,private service:OrgService,private router:Router) {
    this.Organizers = {oname : '',email : '',phonenumber: '',password : ''};
   }

  ngOnInit(): void {
    console.log('Data Recieved ....');
  }

  async registerOrgs(registerForm:any){
    await this.service.getOrgByDetails(registerForm).then((result:any)=>{console.log(result);
      this.Organizers = result
    });
    this.service.setUserLoggedIn();
    if(this.Organizers != null){
      
      this.router.navigate(['orglogin']);
    }
    else{
      alert("Please fill the details");
    }
  }

  
  registerOrg(){
    this.service.registerOrg(this.Organizers).subscribe();
  }
}

