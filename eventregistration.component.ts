import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { UserService } from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-eventregistration',
  templateUrl: './eventregistration.component.html',
  styleUrls: ['./eventregistration.component.css']
})
export class EventregistrationComponent implements OnInit {

  users : any;

  constructor(private httpClient:HttpClient,private service:UserService,private router:Router) {
    this.users = {sname : '',email : '',phonenumber: '',password : ''};
   }

  ngOnInit(): void {
    console.log('Data Recieved ....');
  }

  async registerUsers(registerForm:any){
    await this.service.getUserByDetails(registerForm).then((result:any)=>{console.log(result);
      this.users = result
    });
    this.service.setUserLoggedIn();
    if(this.users != null){
      
      this.router.navigate(['eventlogin']);
    }
    else{
      alert("Please fill the details");
    }
  }

  
  registerUser(){
    this.service.registerUser(this.users).subscribe();
  }
}

