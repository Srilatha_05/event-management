import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-eventlogin',
  templateUrl: './eventlogin.component.html',
  styleUrls: ['./eventlogin.component.css']
})
export class EventloginComponent implements OnInit {
   users: any;

  constructor(private service:UserService,private httpClient:HttpClient,private router:Router) { 
    this.users ={email:"",password:""};
  }

  ngOnInit(): void {
  }
  
  async loginuser(loginForm:any){
    await this.service.getUserByEmailAndPassword(loginForm).then((result:any)=>{console.log(result);
      this.users = result
    });
    this.service.setUserLoggedIn();
    if(this.users != null){
      
      this.router.navigate(['userhomepage']);
    }
    else{
      alert("Invalid User");
    }
  }
  }

  
